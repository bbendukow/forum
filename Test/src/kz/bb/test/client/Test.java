package kz.bb.test.client;

import kz.bb.test.client.dto.IssueItemDTO;
import kz.bb.test.client.dto.StatisticsDTO;
import kz.bb.test.client.dto.StatusDTO;
import kz.bb.test.client.dto.StyleDTO;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

public class Test implements EntryPoint {
	private Factory factory;
	private IssuesGrid grid;
	private static StyleDTO defaultStyle;
	static {
		defaultStyle = new StyleDTO(//
				12, // gridFontSize
				"black", // gridFontColor
				"normal", // gridFontStyle
				"verdana, sans-serif", // gridFontFamily
				"bold", // gridFontWeight
				"white", // gridBackgroundColor
				"black", // gridHighlightedFontColor
				"normal", // gridHighlightedFontStyle
				"verdana, sans-serif", // gridHighlightedFontFamily
				"bold", // gridHighlightedFontWeight
				"#cccccc", // gridHighlightedBackgroundColor
				12, // gridButtonFontSize
				"black", // gridButtonFontColor
				"normal", // gridButtonFontStyle
				"verdana, sans-serif", // gridButtonFontFamily
				"bold", // gridButtonFontWeight
				14, // gridCommentsLabelFontSize
				"blue", // gridCommentsLabelFontColor
				"normal", // gridCommentsLabelFontStyle
				"verdana, sans-serif", // gridCommentsLabelFontFamily
				"normal", // gridCommentsLabelFontWeight
				12, // gridStatusFontSize;
				"white", // gridStatusFontColor;
				"normal", // gridStatusFontStyle;
				"verdana, sans-serif", // gridStatusFontFamily
				"normal", // gridStatusFontWeight
				"grey", // gridStatusBackgroundColor
				"black", // gridHighlightedButtonFontColor
				"normal", // gridHighlightedButtonFontStyle
				"verdana, sans-serif", // gridHighlightedButtonFontFamily
				"bold", // gridHighlightedButtonFontWeight
				"blue", // gridHighlightedCommentsLabelFontColor
				"normal", // gridHighlightedCommentsLabelFontStyle
				"verdana, sans-serif", // gridHighlightedCommentsLabelFontFamily
				"bold", // gridHighlightedCommentsLabelFontWeight
				"black", // gridHighlightedStatusFontColor
				"normal", // gridHighlightedStatusFontStyle
				"verdana, sans-serif", // gridHighlightedStatusFontFamily
				"bold", // gridHighlightedStatusFontWeight
				"#cccccc", // gridHighlightedStatusBackgroundcolor
				12, // gridRatingFontSize
				"black", // gridRatingFontColor
				"normal", // gridRatingFontStyle
				"verdana, sans-serif", // gridRatingFontFamily
				"normal", // gridRatingFontWeight
				"blue", // gridHighlightedRatingFontColor
				"bold", // gridHighlightedRatingFontStyle
				"verdana, sans-serif", // gridHighlightedRatingFontFamily
				"bold", // gridHighlightedRatingFontWeight
				8, // gridVoicesFontSize
				"green", // gridVoicesPositiveFontColor
				"normal", // gridVoicesPositiveFontStyle
				"verdana, sans-serif", // gridVoicesPositiveFontFamily
				"normal", // gridVoicesPositiveFontWeight
				"red", // gridVoicesNegativeFontColor
				"normal", // gridVoicesNegativeFontStyle
				"verdana, sans-serif", // gridVoicesNegativeFontFamily
				"normal", // gridVoicesNegativeFontWeight
				"grey", // gridHighlightedVoicesPositiveFontColor
				"normal", // gridHighlightedVoicesPositiveFontStyle
				"verdana, sans-serif", // gridHighlightedVoicesPositiveFontFamily
				"normal", // gridHighlightedVoicesPositiveFontWeight
				"grey", // gridHighlightedVoicesNegativeFontColor
				"normal", // gridHighlightedVoicesNegativeFontStyle
				"verdana, sans-serif", // gridHighlightedVoicesNegativeFontFamily
				"normal", // gridHighlightedVoicesNegativeFontWeight
				14, // gridTitleFontSize
				"black", // gridTitleFontColor
				"normal", // gridTitleFontStyle
				"verdana, sans-serif", // gridTitleFontFamily
				"normal", // gridTitleFontWeight
				"blue", // gridHighlightedTitleFontColor
				"bold", // gridHighlightedTitleFontStyle
				"verdana, sans-serif", // gridHighlightedTitleFontFamily
				"bold", // gridHighlightedTitleFontWeight
				10, // issueItemHeight
				10, // issueItemMembersMarginLR
				0, // issueItemButtonsPaddingLR
				10, // issueItemPaddingLR
				120, // issueItemStatusElementWidth
				10, // issueItemRatingElementPadding
				25, // issueItemRatingIndicatorLineWidth
				5, // issueItemRatingIndicatorLineHeight
				"green", // issueItemRatingIndicatorPositiveColor;
				"red", // issueItemRatingIndicatorNegativeColor
				"green", // issueItemRatingIndicatorPositiveBorderColor;
				"red", // issueItemRatingIndicatorNegativeBorderColor
				"darkgrey", // issueItemHighlightedRatingIndicatorPositiveColor
				"darkgrey", // issueItemHighlightedRatingIndicatorNegativeColor
				"darkgrey", // issueItemHighlightedRatingIndicatorPositiveBorderColor
				"darkgrey", // issueItemHighlightedRatingIndicatorNegativeBorderColor
				3, // issueItemPaddingTB;
				1, // issueItemSeparatorThickness;
				"dotted", // issueItemSeparatorType;
				"grey", // issueItemSeparatorColor;
				"red" // issueItemHighlightedSeparatorColor;
		);
	}

	public void onModuleLoad() {
		factory = new Factory();

		factory.setStyle(defaultStyle);
		grid = factory.createIssuesGrid();

		{
			IssueItemDTO i = new IssueItemDTO("1", new StatusDTO("Новый"),
					new StatisticsDTO(0, 0, false, false));
			grid.addItem(i);
		}


		RootPanel.get().add(grid);
	}
}
