package kz.bb.test.client.IssueItemElements;

import kz.bb.test.client.Factory;
import kz.bb.test.client.STATE;
import kz.bb.test.client.dto.StatisticsDTO;
import kz.bb.test.client.dto.StyleDTO;

import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class RatingElement extends VerticalPanel {
  private Label value;
  private IndicationElement indicationElement;
  private StyleDTO style;
  
  public RatingElement(Factory factory, StatisticsDTO statisticsDTO) {
    style = factory.getStyle();
    getElement().getStyle().setPaddingLeft(style.getIssueItemRatingElementPadding(), Unit.PX);
    getElement().getStyle().setPaddingRight(style.getIssueItemRatingElementPadding(), Unit.PX);
    
    setWidth("5px");
    setHeight("100%");
    value = new Label("0");
    add(value);
    setCellHorizontalAlignment(value, HasAlignment.ALIGN_CENTER);
    indicationElement = new IndicationElement();
    add(indicationElement);
  }
  
  class IndicationElement extends HorizontalPanel {
    private Indicator rightIndicator;
    private Indicator leftIndicator;
    
    public IndicationElement() {
      setSize("100%", "100%");
      leftIndicator = new Indicator(ALIGNMENT.LEFT);
      rightIndicator = new Indicator(ALIGNMENT.RIGHT);
      rightIndicator.getElement().getStyle().setPaddingLeft(1, Unit.PX);
      add(leftIndicator);
      add(rightIndicator);
    }
    
    class Indicator extends VerticalPanel {
      private Label value;
      private HorizontalPanel line;
      private HorizontalPanel filler;
      private ALIGNMENT alignment;
      
      @SuppressWarnings("unused")
      private Indicator() {}
      
      public Indicator(ALIGNMENT alignment) {
        this.alignment = alignment;
        setSize("100%", "100%");
        line = new HorizontalPanel();
        line.setSize(style.getIssueItemRatingIndicatorLineWidth() + "px",
            style.getIssueItemRatingIndicatorLineHeight() + "px");
        line.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
        line.getElement().getStyle().setBorderWidth(1, Unit.PX);
        filler = new HorizontalPanel();
        filler.setHeight("100%");
        filler.setWidth("0px");
        line.add(filler);
        value = new Label("-1");
        add(line);
        add(value);
        line.setCellHorizontalAlignment(filler, alignment == ALIGNMENT.LEFT ? ALIGN_RIGHT
            :ALIGN_LEFT);
        setCellHorizontalAlignment(line, alignment == ALIGNMENT.LEFT ? ALIGN_RIGHT :ALIGN_LEFT);
        setCellHorizontalAlignment(value, ALIGN_CENTER);
      }
      
      public void updateState(STATE pos) {
        String bkgColor;
        String borderColor;
        String gridVoicesFontColor;
        String gridVoicesFontStyle;
        String gridVoicesFontWeight;
        String gridVoicesFontFamily;
        if (alignment == ALIGNMENT.LEFT) {
          bkgColor = pos.issueItemRatingIndicatorNegativeColor;
          borderColor = pos.issueItemRatingIndicatorNegativeBorderColor;
          gridVoicesFontColor = pos.gridVoicesNegativeFontColor;
          gridVoicesFontStyle = pos.gridVoicesNegativeFontStyle;
          gridVoicesFontWeight = pos.gridVoicesNegativeFontWeight;
          gridVoicesFontFamily = pos.gridVoicesNegativeFontFamily;
        } else {
          bkgColor = pos.issueItemRatingIndicatorPositiveColor;
          borderColor = pos.issueItemRatingIndicatorPositiveBorderColor;
          gridVoicesFontColor = pos.gridVoicesPositiveFontColor;
          gridVoicesFontStyle = pos.gridVoicesPositiveFontStyle;
          gridVoicesFontWeight = pos.gridVoicesPositiveFontWeight;
          gridVoicesFontFamily = pos.gridVoicesPositiveFontFamily;
        }
        filler.getElement().getStyle().setBackgroundColor(bkgColor);
        line.getElement().getStyle().setBorderColor(borderColor);
        
        Element w = value.getElement();
        w.getStyle().setProperty("color", gridVoicesFontColor);
        w.getStyle().setFontSize(pos.gridVoicesFontSize, Unit.PX);
        w.getStyle().setProperty("fontStyle", gridVoicesFontStyle);
        w.getStyle().setProperty("fontWeight", gridVoicesFontWeight);
        w.getStyle().setProperty("fontFamily", gridVoicesFontFamily);
      }
      
      public void setValue(int value, int prc) {
        this.value.setText(value + "");
        Double w = 0D;
        if (prc > 0) {
          w = new Double(style.getIssueItemRatingIndicatorLineWidth()) / 100 * prc;
        }
        filler.setWidth(w.intValue() + "px");
      }
    }
    
    public void updateState(STATE pos) {
      Double val1 = new Double(Random.nextInt(400) + "");
      Double val2 = new Double(Random.nextInt(400) + "");
      int w1 = 0;
      int w2 = 0;
      if (val1 > 0 || val2 > 0) {
        w1 = new Double(val1 / (val1 + val2) * 100).intValue();
        w2 = 100 - w1;
      }
      ;
      leftIndicator.setValue(val1.intValue(), w1);
      rightIndicator.setValue(val2.intValue(), w2);
      leftIndicator.updateState(pos);
      rightIndicator.updateState(pos);
    }
  }
  
  public enum ALIGNMENT {
    LEFT, RIGHT
  }
  
  public void updateState(STATE pos) {
    Element w = value.getElement();
    w.getStyle().setProperty("color", pos.gridRatingFontColor);
    w.getStyle().setFontSize(pos.gridRatingFontSize, Unit.PX);
    w.getStyle().setProperty("fontStyle", pos.gridRatingFontStyle);
    w.getStyle().setProperty("fontWeight", pos.gridRatingFontWeight);
    w.getStyle().setProperty("fontFamily", pos.gridRatingFontFamily);
    indicationElement.updateState(pos);
  }
  
}
