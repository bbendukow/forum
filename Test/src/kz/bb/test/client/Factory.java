package kz.bb.test.client;

import kz.bb.test.client.IssueItemElements.RatingElement;
import kz.bb.test.client.dto.IssueItemDTO;
import kz.bb.test.client.dto.StyleDTO;
public class Factory {
  private IssuesGrid issuesGrid;
  private StyleDTO styleDTO;
  private String currentLanguage;
  
  public IssuesGrid createIssuesGrid() {
    issuesGrid = new IssuesGrid(this);
    return issuesGrid;
  }
  
  public void init(String currentLanguage, StyleDTO styleDTO) {
    this.styleDTO = styleDTO;
    this.currentLanguage = currentLanguage;
  }
  
  public void setStyle(StyleDTO styleDTO) {
    this.styleDTO = styleDTO;
  }
  
  public void updateLanguage(String currentLanguage) {
    this.currentLanguage = currentLanguage;
  }
  
  public StyleDTO getStyle() {
    return styleDTO;
  }
  
  public String getCurrentLanguage() {
    return currentLanguage;
  }
  
  public IssueItem createIssueItem(IssueItemDTO data) {
    IssueItem item = new IssueItem(this, data);
    return item;
  }
  
  public RatingElement createRatingElement(IssueItemDTO data) {
    return new RatingElement(this, data.getStatistics());
  }
  
  public StatusElement createStatusElement(IssueItemDTO data) {
    return new StatusElement(this, data.getStatus());
  }
  
  public ControlElement createControlElement(IssueItemDTO data) {
    return new ControlElement(this, data.getId());
  }
}
