package kz.bb.test.client;

import kz.bb.test.client.dto.IssueItemDTO;
import kz.bb.test.client.dto.StyleDTO;

import com.google.gwt.user.client.ui.VerticalPanel;

public class IssuesGrid extends VerticalPanel {
  private Factory factory;

  public IssuesGrid(Factory factory) {
    this.factory = factory;
    setSize("100%", "100%");
    StyleDTO s = factory.getStyle();
    
    STATE.OUT.init(s.getGridFontSize(), s.getGridFontColor(), s.getGridFontStyle(),
        s.getGridFontFamily(), s.getGridFontFamily(), s.getGridBackgroundColor(),
        s.getGridButtonFontSize(), s.getGridButtonFontColor(), s.getGridButtonFontStyle(),
        s.getGridButtonFontFamily(), s.getGridButtonFontWeight(), s.getGridCommentsLabelFontSize(),
        s.getGridCommentsLabelFontColor(), s.getGridCommentsLabelFontStyle(),
        s.getGridCommentsLabelFontFamily(), s.getGridCommentsLabelFontWeight(),
        s.getGridStatusFontSize(), s.getGridStatusFontColor(), s.getGridStatusFontStyle(),
        s.getGridStatusFontFamily(), s.getGridStatusFontWeight(), s.getGridStatusBackgroundColor(),
        s.getGridRatingFontSize(), s.getGridRatingFontColor(), s.getGridRatingFontStyle(),
        s.getGridRatingFontFamily(), s.getGridRatingFontWeight(), s.getGridVoicesFontSize(),
        s.getGridVoicesPositiveFontColor(), s.getGridVoicesPositiveFontStyle(),
        s.getGridVoicesPositiveFontFamily(), s.getGridVoicesPositiveFontWeight(),
        s.getGridVoicesNegativeFontColor(), s.getGridVoicesNegativeFontStyle(),
        s.getGridVoicesNegativeFontFamily(), s.getGridVoicesNegativeFontWeight(),
        s.getGridTitleFontSize(), s.getGridTitleFontColor(), s.getGridTitleFontStyle(),
        s.getGridTitleFontFamily(), s.getGridTitleFontWeight(),
        s.getIssueItemRatingIndicatorPositiveColor(), s.getIssueItemRatingIndicatorNegativeColor(),
        s.getIssueItemRatingIndicatorPositiveBorderColor(),
        s.getIssueItemRatingIndicatorNegativeBorderColor(), s.getIssueItemSeparatorColor());
    STATE.OVER.init(s.getGridFontSize(), s.getGridHighlightedFontColor(),
        s.getGridHighlightedFontStyle(), s.getGridHighlightedFontFamily(),
        s.getGridHighlightedFontFamily(), s.getGridHighlightedBackgroundColor(),
        s.getGridButtonFontSize(), s.getGridHighlightedButtonFontColor(),
        s.getGridHighlightedButtonFontStyle(), s.getGridHighlightedButtonFontFamily(),
        s.getGridHighlightedButtonFontWeight(), s.getGridCommentsLabelFontSize(),
        s.getGridHighlightedCommentsLabelFontColor(), s.getGridHighlightedCommentsLabelFontStyle(),
        s.getGridHighlightedCommentsLabelFontFamily(),
        s.getGridHighlightedCommentsLabelFontWeight(), s.getGridStatusFontSize(),
        s.getGridHighlightedStatusFontColor(), s.getGridHighlightedStatusFontStyle(),
        s.getGridHighlightedStatusFontFamily(), s.getGridHighlightedStatusFontWeight(),
        s.getGridHighlightedStatusBackgroundColor(), s.getGridRatingFontSize(),
        s.getGridHighlightedRatingFontColor(), s.getGridHighlightedRatingFontStyle(),
        s.getGridHighlightedRatingFontFamily(), s.getGridHighlightedRatingFontWeight(),
        s.getGridVoicesFontSize(), s.getGridHighlightedVoicesPositiveFontColor(),
        s.getGridHighlightedVoicesPositiveFontStyle(),
        s.getGridHighlightedVoicesPositiveFontFamily(),
        s.getGridHighlightedVoicesPositiveFontWeight(),
        s.getGridHighlightedVoicesNegativeFontColor(),
        s.getGridHighlightedVoicesNegativeFontStyle(),
        s.getGridHighlightedVoicesNegativeFontFamily(),
        s.getGridHighlightedVoicesNegativeFontWeight(), s.getGridTitleFontSize(),
        s.getGridHighlightedTitleFontColor(), s.getGridHighlightedTitleFontStyle(),
        s.getGridHighlightedTitleFontFamily(), s.getGridHighlightedTitleFontWeight(),
        s.getIssueItemHighlightedRatingIndicatorPositiveColor(),
        s.getIssueItemHighlightedRatingIndicatorNegativeColor(),
        s.getIssueItemHighlightedRatingIndicatorPositiveBorderColor(),
        s.getIssueItemHighlightedRatingIndicatorNegativeBorderColor(),
        s.getIssueItemHighlightedSeparatorColor());
  }
  
  public void updateStyle(StyleDTO style) {
    
  }
  
  public void addItem(IssueItemDTO data) {
    add(factory.createIssueItem(data));
  };
}
