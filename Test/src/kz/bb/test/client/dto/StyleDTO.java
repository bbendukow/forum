package kz.bb.test.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

public class StyleDTO implements IsSerializable {
  private Integer gridFontSize;
  private String gridFontColor;
  private String gridFontStyle;
  private String gridFontFamily;
  private String gridFontWeight;
  private String gridBackgroundColor;
  private String gridHighlightedFontColor;
  private String gridHighlightedFontStyle;
  private String gridHighlightedFontFamily;
  private String gridHighlightedFontWeight;
  private String gridHighlightedBackgroundColor;
  private Integer gridButtonFontSize;
  private String gridButtonFontColor;
  private String gridButtonFontStyle;
  private String gridButtonFontFamily;
  private String gridButtonFontWeight;
  private Integer gridCommentsLabelFontSize;
  private String gridCommentsLabelFontColor;
  private String gridCommentsLabelFontStyle;
  private String gridCommentsLabelFontFamily;
  private String gridCommentsLabelFontWeight;
  private Integer gridStatusFontSize;
  private String gridStatusFontColor;
  private String gridStatusFontStyle;
  private String gridStatusFontFamily;
  private String gridStatusFontWeight;
  private String gridStatusBackgroundColor;
  private String gridHighlightedButtonFontColor;
  private String gridHighlightedButtonFontStyle;
  private String gridHighlightedButtonFontFamily;
  private String gridHighlightedButtonFontWeight;
  private String gridHighlightedCommentsLabelFontColor;
  private String gridHighlightedCommentsLabelFontStyle;
  private String gridHighlightedCommentsLabelFontFamily;
  private String gridHighlightedCommentsLabelFontWeight;
  private String gridHighlightedStatusFontColor;
  private String gridHighlightedStatusFontStyle;
  private String gridHighlightedStatusFontFamily;
  private String gridHighlightedStatusFontWeight;
  private String gridHighlightedStatusBackgroundColor;
  private Integer gridRatingFontSize;
  private String gridRatingFontColor;
  private String gridRatingFontStyle;
  private String gridRatingFontFamily;
  private String gridRatingFontWeight;
  private String gridHighlightedRatingFontColor;
  private String gridHighlightedRatingFontStyle;
  private String gridHighlightedRatingFontFamily;
  private String gridHighlightedRatingFontWeight;
  private Integer gridVoicesFontSize;
  private String gridVoicesPositiveFontColor;
  private String gridVoicesPositiveFontStyle;
  private String gridVoicesPositiveFontFamily;
  private String gridVoicesPositiveFontWeight;
  private String gridVoicesNegativeFontColor;
  private String gridVoicesNegativeFontStyle;
  private String gridVoicesNegativeFontFamily;
  private String gridVoicesNegativeFontWeight;
  private String gridHighlightedVoicesPositiveFontColor;
  private String gridHighlightedVoicesPositiveFontStyle;
  private String gridHighlightedVoicesPositiveFontFamily;
  private String gridHighlightedVoicesPositiveFontWeight;
  private String gridHighlightedVoicesNegativeFontColor;
  private String gridHighlightedVoicesNegativeFontStyle;
  private String gridHighlightedVoicesNegativeFontFamily;
  private String gridHighlightedVoicesNegativeFontWeight;
  private Integer gridTitleFontSize;
  private String gridTitleFontColor;
  private String gridTitleFontStyle;
  private String gridTitleFontFamily;
  private String gridTitleFontWeight;
  private String gridHighlightedTitleFontColor;
  private String gridHighlightedTitleFontStyle;
  private String gridHighlightedTitleFontFamily;
  private String gridHighlightedTitleFontWeight;
  private Integer issueItemHeight;
  private Integer issueItemMembersMarginLR;
  private Integer issueItemButtonsPaddingLR;
  private Integer issueItemPaddingLR;
  private Integer issueItemStatusElementWidth;
  private Integer issueItemRatingElementPadding;
  private Integer issueItemRatingIndicatorLineWidth;
  private Integer issueItemRatingIndicatorLineHeight;
  private String issueItemRatingIndicatorPositiveColor;
  private String issueItemRatingIndicatorNegativeColor;
  private String issueItemRatingIndicatorPositiveBorderColor;
  private String issueItemRatingIndicatorNegativeBorderColor;
  private String issueItemHighlightedRatingIndicatorPositiveColor;
  private String issueItemHighlightedRatingIndicatorNegativeColor;
  private String issueItemHighlightedRatingIndicatorPositiveBorderColor;
  private String issueItemHighlightedRatingIndicatorNegativeBorderColor;
  private Integer issueItemPaddingTB;
  private Integer issueItemSeparatorThickness;
  private String issueItemSeparatorType;
  private String issueItemSeparatorColor;
  private String issueItemHighlightedSeparatorColor;
  
  @SuppressWarnings("unused")
  private StyleDTO() {}
  
  public StyleDTO(Integer gridFontSize, String gridFontColor, String gridFontStyle,
      String gridFontFamily, String gridFontWeight, String gridBackgroundColor,
      String gridHighlightedFontColor, String gridHighlightedFontStyle,
      String gridHighlightedFontFamily, String gridHighlightedFontWeight,
      String gridHighlightedBackgroundColor, Integer gridButtonFontSize,
      String gridButtonFontColor, String gridButtonFontStyle, String gridButtonFontFamily,
      String gridButtonFontWeight, Integer gridCommentsLabelFontSize,
      String gridCommentsLabelFontColor, String gridCommentsLabelFontStyle,
      String gridCommentsLabelFontFamily, String gridCommentsLabelFontWeight,
      Integer gridStatusFontSize, String gridStatusFontColor, String gridStatusFontStyle,
      String gridStatusFontFamily, String gridStatusFontWeight, String gridStatusBackgroundColor,
      String gridHighlightedButtonFontColor, String gridHighlightedButtonFontStyle,
      String gridHighlightedButtonFontFamily, String gridHighlightedButtonFontWeight,
      String gridHighlightedCommentsLabelFontColor, String gridHighlightedCommentsLabelFontStyle,
      String gridHighlightedCommentsLabelFontFamily, String gridHighlightedCommentsLabelFontWeight,
      String gridHighlightedStatusFontColor, String gridHighlightedStatusFontStyle,
      String gridHighlightedStatusFontFamily, String gridHighlightedStatusFontWeight,
      String gridHighlightedStatusBackgroundColor, Integer gridRatingFontSize,
      String gridRatingFontColor, String gridRatingFontStyle, String gridRatingFontFamily,
      String gridRatingFontWeight, String gridHighlightedRatingFontColor,
      String gridHighlightedRatingFontStyle, String gridHighlightedRatingFontFamily,
      String gridHighlightedRatingFontWeight, Integer gridVoicesFontSize,
      String gridVoicesPositiveFontColor, String gridVoicesPositiveFontStyle,
      String gridVoicesPositiveFontFamily, String gridVoicesPositiveFontWeight,
      String gridVoicesNegativeFontColor, String gridVoicesNegativeFontStyle,
      String gridVoicesNegativeFontFamily, String gridVoicesNegativeFontWeight,
      String gridHighlightedVoicesPositiveFontColor, String gridHighlightedVoicesPositiveFontStyle,
      String gridHighlightedVoicesPositiveFontFamily,
      String gridHighlightedVoicesPositiveFontWeight,
      String gridHighlightedVoicesNegativeFontColor, String gridHighlightedVoicesNegativeFontStyle,
      String gridHighlightedVoicesNegativeFontFamily,
      String gridHighlightedVoicesNegativeFontWeight, Integer gridTitleFontSize,
      String gridTitleFontColor, String gridTitleFontStyle, String gridTitleFontFamily,
      String gridTitleFontWeight, String gridHighlightedTitleFontColor,
      String gridHighlightedTitleFontStyle, String gridHighlightedTitleFontFamily,
      String gridHighlightedTitleFontWeight, Integer issueItemHeight,
      Integer issueItemMembersMarginLR, Integer issueItemButtonsPaddingLR,
      Integer issueItemPaddingLR, Integer issueItemStatusElementWidth,
      Integer issueItemRatingElementPadding, Integer issueItemRatingIndicatorLineWidth,
      Integer issueItemRatingIndicatorLineHeight, String issueItemRatingIndicatorPositiveColor,
      String issueItemRatingIndicatorNegativeColor,
      String issueItemRatingIndicatorPositiveBorderColor,
      String issueItemRatingIndicatorNegativeBorderColor,
      String issueItemHighlightedRatingIndicatorPositiveColor,
      String issueItemHighlightedRatingIndicatorNegativeColor,
      String issueItemHighlightedRatingIndicatorPositiveBorderColor,
      String issueItemHighlightedRatingIndicatorNegativeBorderColor, Integer issueItemPaddingTB,
      Integer issueItemSeparatorThickness, String issueItemSeparatorType,
      String issueItemSeparatorColor, String issueItemHighlightedSeparatorColor) {
    super();
    this.gridFontSize = gridFontSize;
    this.gridFontColor = gridFontColor;
    this.gridFontStyle = gridFontStyle;
    this.gridFontFamily = gridFontFamily;
    this.gridFontWeight = gridFontWeight;
    this.gridBackgroundColor = gridBackgroundColor;
    this.gridHighlightedFontColor = gridHighlightedFontColor;
    this.gridHighlightedFontStyle = gridHighlightedFontStyle;
    this.gridHighlightedFontFamily = gridHighlightedFontFamily;
    this.gridHighlightedFontWeight = gridHighlightedFontWeight;
    this.gridHighlightedBackgroundColor = gridHighlightedBackgroundColor;
    this.gridButtonFontSize = gridButtonFontSize;
    this.gridButtonFontColor = gridButtonFontColor;
    this.gridButtonFontStyle = gridButtonFontStyle;
    this.gridButtonFontFamily = gridButtonFontFamily;
    this.gridButtonFontWeight = gridButtonFontWeight;
    this.gridCommentsLabelFontSize = gridCommentsLabelFontSize;
    this.gridCommentsLabelFontColor = gridCommentsLabelFontColor;
    this.gridCommentsLabelFontStyle = gridCommentsLabelFontStyle;
    this.gridCommentsLabelFontFamily = gridCommentsLabelFontFamily;
    this.gridCommentsLabelFontWeight = gridCommentsLabelFontWeight;
    this.gridStatusFontSize = gridStatusFontSize;
    this.gridStatusFontColor = gridStatusFontColor;
    this.gridStatusFontStyle = gridStatusFontStyle;
    this.gridStatusFontFamily = gridStatusFontFamily;
    this.gridStatusFontWeight = gridStatusFontWeight;
    this.gridStatusBackgroundColor = gridStatusBackgroundColor;
    this.gridHighlightedButtonFontColor = gridHighlightedButtonFontColor;
    this.gridHighlightedButtonFontStyle = gridHighlightedButtonFontStyle;
    this.gridHighlightedButtonFontFamily = gridHighlightedButtonFontFamily;
    this.gridHighlightedButtonFontWeight = gridHighlightedButtonFontWeight;
    this.gridHighlightedCommentsLabelFontColor = gridHighlightedCommentsLabelFontColor;
    this.gridHighlightedCommentsLabelFontStyle = gridHighlightedCommentsLabelFontStyle;
    this.gridHighlightedCommentsLabelFontFamily = gridHighlightedCommentsLabelFontFamily;
    this.gridHighlightedCommentsLabelFontWeight = gridHighlightedCommentsLabelFontWeight;
    this.gridHighlightedStatusFontColor = gridHighlightedStatusFontColor;
    this.gridHighlightedStatusFontStyle = gridHighlightedStatusFontStyle;
    this.gridHighlightedStatusFontFamily = gridHighlightedStatusFontFamily;
    this.gridHighlightedStatusFontWeight = gridHighlightedStatusFontWeight;
    this.gridHighlightedStatusBackgroundColor = gridHighlightedStatusBackgroundColor;
    this.gridRatingFontSize = gridRatingFontSize;
    this.gridRatingFontColor = gridRatingFontColor;
    this.gridRatingFontStyle = gridRatingFontStyle;
    this.gridRatingFontFamily = gridRatingFontFamily;
    this.gridRatingFontWeight = gridRatingFontWeight;
    this.gridHighlightedRatingFontColor = gridHighlightedRatingFontColor;
    this.gridHighlightedRatingFontStyle = gridHighlightedRatingFontStyle;
    this.gridHighlightedRatingFontFamily = gridHighlightedRatingFontFamily;
    this.gridHighlightedRatingFontWeight = gridHighlightedRatingFontWeight;
    this.gridVoicesFontSize = gridVoicesFontSize;
    this.gridVoicesPositiveFontColor = gridVoicesPositiveFontColor;
    this.gridVoicesPositiveFontStyle = gridVoicesPositiveFontStyle;
    this.gridVoicesPositiveFontFamily = gridVoicesPositiveFontFamily;
    this.gridVoicesPositiveFontWeight = gridVoicesPositiveFontWeight;
    this.gridVoicesNegativeFontColor = gridVoicesNegativeFontColor;
    this.gridVoicesNegativeFontStyle = gridVoicesNegativeFontStyle;
    this.gridVoicesNegativeFontFamily = gridVoicesNegativeFontFamily;
    this.gridVoicesNegativeFontWeight = gridVoicesNegativeFontWeight;
    this.gridHighlightedVoicesPositiveFontColor = gridHighlightedVoicesPositiveFontColor;
    this.gridHighlightedVoicesPositiveFontStyle = gridHighlightedVoicesPositiveFontStyle;
    this.gridHighlightedVoicesPositiveFontFamily = gridHighlightedVoicesPositiveFontFamily;
    this.gridHighlightedVoicesPositiveFontWeight = gridHighlightedVoicesPositiveFontWeight;
    this.gridHighlightedVoicesNegativeFontColor = gridHighlightedVoicesNegativeFontColor;
    this.gridHighlightedVoicesNegativeFontStyle = gridHighlightedVoicesNegativeFontStyle;
    this.gridHighlightedVoicesNegativeFontFamily = gridHighlightedVoicesNegativeFontFamily;
    this.gridHighlightedVoicesNegativeFontWeight = gridHighlightedVoicesNegativeFontWeight;
    this.gridTitleFontSize = gridTitleFontSize;
    this.gridTitleFontColor = gridTitleFontColor;
    this.gridTitleFontStyle = gridTitleFontStyle;
    this.gridTitleFontFamily = gridTitleFontFamily;
    this.gridTitleFontWeight = gridTitleFontWeight;
    this.gridHighlightedTitleFontColor = gridHighlightedTitleFontColor;
    this.gridHighlightedTitleFontStyle = gridHighlightedTitleFontStyle;
    this.gridHighlightedTitleFontFamily = gridHighlightedTitleFontFamily;
    this.gridHighlightedTitleFontWeight = gridHighlightedTitleFontWeight;
    this.issueItemHeight = issueItemHeight;
    this.issueItemMembersMarginLR = issueItemMembersMarginLR;
    this.issueItemButtonsPaddingLR = issueItemButtonsPaddingLR;
    this.issueItemPaddingLR = issueItemPaddingLR;
    this.issueItemStatusElementWidth = issueItemStatusElementWidth;
    this.issueItemRatingElementPadding = issueItemRatingElementPadding;
    this.issueItemRatingIndicatorLineWidth = issueItemRatingIndicatorLineWidth;
    this.issueItemRatingIndicatorLineHeight = issueItemRatingIndicatorLineHeight;
    this.issueItemRatingIndicatorPositiveColor = issueItemRatingIndicatorPositiveColor;
    this.issueItemRatingIndicatorNegativeColor = issueItemRatingIndicatorNegativeColor;
    this.issueItemRatingIndicatorPositiveBorderColor = issueItemRatingIndicatorPositiveBorderColor;
    this.issueItemRatingIndicatorNegativeBorderColor = issueItemRatingIndicatorNegativeBorderColor;
    this.issueItemHighlightedRatingIndicatorPositiveColor = issueItemHighlightedRatingIndicatorPositiveColor;
    this.issueItemHighlightedRatingIndicatorNegativeColor = issueItemHighlightedRatingIndicatorNegativeColor;
    this.issueItemHighlightedRatingIndicatorPositiveBorderColor = issueItemHighlightedRatingIndicatorPositiveBorderColor;
    this.issueItemHighlightedRatingIndicatorNegativeBorderColor = issueItemHighlightedRatingIndicatorNegativeBorderColor;
    this.issueItemPaddingTB = issueItemPaddingTB;
    this.issueItemSeparatorThickness = issueItemSeparatorThickness;
    this.issueItemSeparatorType = issueItemSeparatorType;
    this.issueItemSeparatorColor = issueItemSeparatorColor;
    this.issueItemHighlightedSeparatorColor = issueItemHighlightedSeparatorColor;
  }
  
  public Integer getIssueItemSeparatorThickness() {
    return issueItemSeparatorThickness;
  }
  
  public Integer getGridFontSize() {
    return gridFontSize;
  }
  
  public String getGridFontColor() {
    return gridFontColor;
  }
  
  public String getGridFontStyle() {
    return gridFontStyle;
  }
  
  public String getGridFontFamily() {
    return gridFontFamily;
  }
  
  public String getGridFontWeight() {
    return gridFontWeight;
  }
  
  public String getGridBackgroundColor() {
    return gridBackgroundColor;
  }
  
  public String getGridHighlightedFontColor() {
    return gridHighlightedFontColor;
  }
  
  public String getGridHighlightedFontStyle() {
    return gridHighlightedFontStyle;
  }
  
  public String getGridHighlightedFontFamily() {
    return gridHighlightedFontFamily;
  }
  
  public String getGridHighlightedFontWeight() {
    return gridHighlightedFontWeight;
  }
  
  public String getGridHighlightedBackgroundColor() {
    return gridHighlightedBackgroundColor;
  }
  
  public Integer getGridButtonFontSize() {
    return gridButtonFontSize;
  }
  
  public String getGridButtonFontColor() {
    return gridButtonFontColor;
  }
  
  public String getGridButtonFontStyle() {
    return gridButtonFontStyle;
  }
  
  public String getGridButtonFontFamily() {
    return gridButtonFontFamily;
  }
  
  public String getGridButtonFontWeight() {
    return gridButtonFontWeight;
  }
  
  public Integer getGridCommentsLabelFontSize() {
    return gridCommentsLabelFontSize;
  }
  
  public String getGridCommentsLabelFontColor() {
    return gridCommentsLabelFontColor;
  }
  
  public String getGridCommentsLabelFontStyle() {
    return gridCommentsLabelFontStyle;
  }
  
  public String getGridCommentsLabelFontFamily() {
    return gridCommentsLabelFontFamily;
  }
  
  public String getGridCommentsLabelFontWeight() {
    return gridCommentsLabelFontWeight;
  }
  
  public Integer getGridStatusFontSize() {
    return gridStatusFontSize;
  }
  
  public String getGridStatusFontColor() {
    return gridStatusFontColor;
  }
  
  public String getGridStatusFontStyle() {
    return gridStatusFontStyle;
  }
  
  public String getGridStatusFontFamily() {
    return gridStatusFontFamily;
  }
  
  public String getGridStatusFontWeight() {
    return gridStatusFontWeight;
  }
  
  public String getGridStatusBackgroundColor() {
    return gridStatusBackgroundColor;
  }
  
  public String getGridHighlightedButtonFontColor() {
    return gridHighlightedButtonFontColor;
  }
  
  public String getGridHighlightedButtonFontStyle() {
    return gridHighlightedButtonFontStyle;
  }
  
  public String getGridHighlightedButtonFontFamily() {
    return gridHighlightedButtonFontFamily;
  }
  
  public String getGridHighlightedButtonFontWeight() {
    return gridHighlightedButtonFontWeight;
  }
  
  public String getGridHighlightedCommentsLabelFontColor() {
    return gridHighlightedCommentsLabelFontColor;
  }
  
  public String getGridHighlightedCommentsLabelFontStyle() {
    return gridHighlightedCommentsLabelFontStyle;
  }
  
  public String getGridHighlightedCommentsLabelFontFamily() {
    return gridHighlightedCommentsLabelFontFamily;
  }
  
  public String getGridHighlightedCommentsLabelFontWeight() {
    return gridHighlightedCommentsLabelFontWeight;
  }
  
  public String getGridHighlightedStatusFontColor() {
    return gridHighlightedStatusFontColor;
  }
  
  public String getGridHighlightedStatusFontStyle() {
    return gridHighlightedStatusFontStyle;
  }
  
  public String getGridHighlightedStatusFontFamily() {
    return gridHighlightedStatusFontFamily;
  }
  
  public String getGridHighlightedStatusFontWeight() {
    return gridHighlightedStatusFontWeight;
  }
  
  public String getGridHighlightedStatusBackgroundColor() {
    return gridHighlightedStatusBackgroundColor;
  }
  
  public Integer getGridRatingFontSize() {
    return gridRatingFontSize;
  }
  
  public String getGridRatingFontColor() {
    return gridRatingFontColor;
  }
  
  public String getGridRatingFontStyle() {
    return gridRatingFontStyle;
  }
  
  public String getGridRatingFontFamily() {
    return gridRatingFontFamily;
  }
  
  public String getGridRatingFontWeight() {
    return gridRatingFontWeight;
  }
  
  public String getGridHighlightedRatingFontColor() {
    return gridHighlightedRatingFontColor;
  }
  
  public String getGridHighlightedRatingFontStyle() {
    return gridHighlightedRatingFontStyle;
  }
  
  public String getGridHighlightedRatingFontFamily() {
    return gridHighlightedRatingFontFamily;
  }
  
  public String getGridHighlightedRatingFontWeight() {
    return gridHighlightedRatingFontWeight;
  }
  
  public Integer getGridVoicesFontSize() {
    return gridVoicesFontSize;
  }
  
  public String getGridVoicesPositiveFontColor() {
    return gridVoicesPositiveFontColor;
  }
  
  public String getGridVoicesPositiveFontStyle() {
    return gridVoicesPositiveFontStyle;
  }
  
  public String getGridVoicesPositiveFontFamily() {
    return gridVoicesPositiveFontFamily;
  }
  
  public String getGridVoicesPositiveFontWeight() {
    return gridVoicesPositiveFontWeight;
  }
  
  public String getGridVoicesNegativeFontColor() {
    return gridVoicesNegativeFontColor;
  }
  
  public String getGridVoicesNegativeFontStyle() {
    return gridVoicesNegativeFontStyle;
  }
  
  public String getGridVoicesNegativeFontFamily() {
    return gridVoicesNegativeFontFamily;
  }
  
  public String getGridVoicesNegativeFontWeight() {
    return gridVoicesNegativeFontWeight;
  }
  
  public String getGridHighlightedVoicesPositiveFontColor() {
    return gridHighlightedVoicesPositiveFontColor;
  }
  
  public String getGridHighlightedVoicesPositiveFontStyle() {
    return gridHighlightedVoicesPositiveFontStyle;
  }
  
  public String getGridHighlightedVoicesPositiveFontFamily() {
    return gridHighlightedVoicesPositiveFontFamily;
  }
  
  public String getGridHighlightedVoicesPositiveFontWeight() {
    return gridHighlightedVoicesPositiveFontWeight;
  }
  
  public String getGridHighlightedVoicesNegativeFontColor() {
    return gridHighlightedVoicesNegativeFontColor;
  }
  
  public String getGridHighlightedVoicesNegativeFontStyle() {
    return gridHighlightedVoicesNegativeFontStyle;
  }
  
  public String getGridHighlightedVoicesNegativeFontFamily() {
    return gridHighlightedVoicesNegativeFontFamily;
  }
  
  public String getGridHighlightedVoicesNegativeFontWeight() {
    return gridHighlightedVoicesNegativeFontWeight;
  }
  
  public Integer getGridTitleFontSize() {
    return gridTitleFontSize;
  }
  
  public String getGridTitleFontColor() {
    return gridTitleFontColor;
  }
  
  public String getGridTitleFontStyle() {
    return gridTitleFontStyle;
  }
  
  public String getGridTitleFontFamily() {
    return gridTitleFontFamily;
  }
  
  public String getGridTitleFontWeight() {
    return gridTitleFontWeight;
  }
  
  public String getGridHighlightedTitleFontColor() {
    return gridHighlightedTitleFontColor;
  }
  
  public String getGridHighlightedTitleFontStyle() {
    return gridHighlightedTitleFontStyle;
  }
  
  public String getGridHighlightedTitleFontFamily() {
    return gridHighlightedTitleFontFamily;
  }
  
  public String getGridHighlightedTitleFontWeight() {
    return gridHighlightedTitleFontWeight;
  }
  
  public Integer getIssueItemHeight() {
    return issueItemHeight;
  }
  
  public Integer getIssueItemMembersMarginLR() {
    return issueItemMembersMarginLR;
  }
  
  public Integer getIssueItemButtonsPaddingLR() {
    return issueItemButtonsPaddingLR;
  }
  
  public Integer getIssueItemPaddingLR() {
    return issueItemPaddingLR;
  }
  
  public Integer getIssueItemStatusElementWidth() {
    return issueItemStatusElementWidth;
  }
  
  public Integer getIssueItemRatingElementPadding() {
    return issueItemRatingElementPadding;
  }
  
  public Integer getIssueItemRatingIndicatorLineWidth() {
    return issueItemRatingIndicatorLineWidth;
  }
  
  public Integer getIssueItemRatingIndicatorLineHeight() {
    return issueItemRatingIndicatorLineHeight;
  }
  
  public String getIssueItemRatingIndicatorPositiveColor() {
    return issueItemRatingIndicatorPositiveColor;
  }
  
  public String getIssueItemRatingIndicatorNegativeColor() {
    return issueItemRatingIndicatorNegativeColor;
  }
  
  public String getIssueItemRatingIndicatorPositiveBorderColor() {
    return issueItemRatingIndicatorPositiveBorderColor;
  }
  
  public String getIssueItemRatingIndicatorNegativeBorderColor() {
    return issueItemRatingIndicatorNegativeBorderColor;
  }
  
  public String getIssueItemHighlightedRatingIndicatorPositiveColor() {
    return issueItemHighlightedRatingIndicatorPositiveColor;
  }
  
  public String getIssueItemHighlightedRatingIndicatorNegativeColor() {
    return issueItemHighlightedRatingIndicatorNegativeColor;
  }
  
  public String getIssueItemHighlightedRatingIndicatorPositiveBorderColor() {
    return issueItemHighlightedRatingIndicatorPositiveBorderColor;
  }
  
  public String getIssueItemHighlightedRatingIndicatorNegativeBorderColor() {
    return issueItemHighlightedRatingIndicatorNegativeBorderColor;
  }
  
  public String getIssueItemHighlightedSeparatorColor() {
    return issueItemHighlightedSeparatorColor;
  }
  
  public Integer getIssueItemPaddingTB() {
    return issueItemPaddingTB;
  }
  
  public String getIssueItemSeparatorColor() {
    return issueItemSeparatorColor;
  }
  
  public String getIssueItemSeparatorType() {
    return issueItemSeparatorType;
  }
}
