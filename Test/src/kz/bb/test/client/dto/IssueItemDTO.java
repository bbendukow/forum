package kz.bb.test.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

public class IssueItemDTO implements IsSerializable {
  private String id;
  private StatusDTO status;
  private StatisticsDTO statistics;
  
  @SuppressWarnings("unused")
  private IssueItemDTO() {}
  
  public IssueItemDTO(String id, StatusDTO status, StatisticsDTO statistics) {
    super();
    this.id = id;
    this.statistics = statistics;
    this.status = status;
  }
  
  public String getId() {
    return id;
  }
  
  public StatusDTO getStatus() {
    return status;
  }
  
  public StatisticsDTO getStatistics() {
    return statistics;
  }
}
