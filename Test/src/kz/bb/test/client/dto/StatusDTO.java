package kz.bb.test.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

public class StatusDTO implements IsSerializable {
  String name;
  
  @SuppressWarnings("unused")
  private StatusDTO() {}
  
  public StatusDTO(String name) {
    super();
    this.name = name;
  }
  
  public String getName() {
    return name;
  }
  
}
