package kz.bb.test.client.dto;

public class StatisticsDTO {
  private Integer positiveVoicesCount;
  private Integer negotiveVoicesCount;
  private Boolean isLiked;
  private Boolean isDisliked;
  
  private StatisticsDTO() {}
  
  public StatisticsDTO(Integer positiveVoicesCount, Integer negotiveVoicesCount, Boolean isLiked,
      Boolean isDisliked) {
    super();
    this.positiveVoicesCount = positiveVoicesCount;
    this.negotiveVoicesCount = negotiveVoicesCount;
    this.isLiked = isLiked;
    this.isDisliked = isDisliked;
  }
  
  public Boolean isDisliked() {
    return isDisliked;
  }
  
  public Boolean isLiked() {
    return isLiked;
  }
  
  public Integer getPositiveVoicesCount() {
    return positiveVoicesCount;
  }
  
  public Integer getNegotiveVoicesCount() {
    return negotiveVoicesCount;
  }
}
