package kz.bb.test.client;

import kz.bb.test.client.dto.StyleDTO;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class ControlElement extends VerticalPanel {
  private Label title;
  private ControlPanel controlPanel;
  private Factory factory;
  private StyleDTO style;
  
  public ControlElement(Factory factory, String token) {
    this.factory = factory;
    style = factory.getStyle();
    getElement().getStyle().setPaddingLeft(style.getIssueItemPaddingLR(), Unit.PX);
    getElement().getStyle().setPaddingRight(style.getIssueItemPaddingLR(), Unit.PX);
    
    setWidth("5px");
    setHeight("100%");
    
    title = new Label("Title");
    controlPanel = new ControlPanel(token);
    
    add(title);
    add(controlPanel);
  }
  
  class ControlPanel extends HorizontalPanel {
    private Button btnLike;
    private Button btnDislike;
    private Hyperlink commentsLink;
    
    public ControlPanel(String token) {
      setSize("1px", "100%");
      
      btnLike = new Button("<img src='Images/Like.png'>" + "Like" + "</img>");
      btnDislike = new Button("<img src='Images/Dislike.png'>" + "Dislike" + "</img>");
      commentsLink = new Hyperlink("0 comments", token);
      
      add(btnLike);
      add(btnDislike);
      add(commentsLink);
      
      commentsLink.getElement().getStyle().setProperty("whiteSpace", "nowrap");
      btnLike.getElement().getStyle().setProperty("whiteSpace", "nowrap");
      btnDislike.getElement().getStyle().setProperty("whiteSpace", "nowrap");
      
      btnLike.getElement().getStyle().setPaddingLeft(style.getIssueItemButtonsPaddingLR(), Unit.PX);
      btnLike.getElement().getStyle()
          .setPaddingRight(style.getIssueItemButtonsPaddingLR(), Unit.PX);
      btnDislike.getElement().getStyle()
          .setPaddingLeft(style.getIssueItemButtonsPaddingLR(), Unit.PX);
      btnDislike.getElement().getStyle()
          .setPaddingRight(style.getIssueItemButtonsPaddingLR(), Unit.PX);
      
      btnDislike.getElement().getStyle()
          .setMarginLeft(style.getIssueItemMembersMarginLR(), Unit.PX);
      commentsLink.getElement().getStyle()
          .setMarginLeft(style.getIssueItemMembersMarginLR(), Unit.PX);
      
      setCellHorizontalAlignment(btnLike, HasAlignment.ALIGN_LEFT);
      setCellHorizontalAlignment(btnDislike, HasAlignment.ALIGN_LEFT);
      setCellHorizontalAlignment(commentsLink, HasAlignment.ALIGN_LEFT);
      setCellVerticalAlignment(commentsLink, HasAlignment.ALIGN_MIDDLE);
    }
    
    public void updateState(STATE pos) {
      Widget[] widgets = new Widget[] { btnLike, btnDislike };
      for (int i = 0; i < widgets.length; i++) {
        Element w = widgets[i].getElement();
        w.getStyle().setProperty("color", pos.buttonFontColor);
        w.getStyle().setFontSize(pos.buttonFontSize, Unit.PX);
        w.getStyle().setProperty("fontStyle", pos.buttonFontStyle);
        w.getStyle().setProperty("fontWeight", pos.buttonFontWeight);
        w.getStyle().setProperty("fontFamily", pos.buttonFontFamily);
      }
      Element w = commentsLink.getElement();
      w.getStyle().setProperty("color", pos.gridCommentsLabelFontColor);
      w.getStyle().setFontSize(pos.gridCommentsLabelFontSize, Unit.PX);
      w.getStyle().setProperty("fontStyle", pos.gridCommentsLabelFontStyle);
      w.getStyle().setProperty("fontWeight", pos.gridCommentsLabelFontWeight);
      w.getStyle().setProperty("fontFamily", pos.gridCommentsLabelFontFamily);
    }
  }
  
  public void updateState(STATE pos) {
    Element w = title.getElement();
    w.getStyle().setProperty("color", pos.gridTitleFontColor);
    w.getStyle().setFontSize(pos.gridTitleFontSize, Unit.PX);
    w.getStyle().setProperty("fontStyle", pos.gridTitleFontStyle);
    w.getStyle().setProperty("fontWeight", pos.gridTitleFontWeight);
    w.getStyle().setProperty("fontFamily", pos.gridTitleFontFamily);
    controlPanel.updateState(pos);
  }
}
