package kz.bb.test.client;

import kz.bb.test.client.IssueItemElements.RatingElement;
import kz.bb.test.client.dto.IssueItemDTO;
import kz.bb.test.client.dto.StyleDTO;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;

public class IssueItem extends HorizontalPanel {
  private STATE mousePosition;
  private StatusElement statusElement;
  private ControlElement controlElement;
  private RatingElement ratingElement;
  private Factory factory;
  private StyleDTO style;
  
  public IssueItem(Factory factory, IssueItemDTO data) {
    this.factory = factory;
    style = factory.getStyle();
    setSize("100%", style.getIssueItemHeight() + "px");
    getElement().getStyle().setPaddingTop(style.getIssueItemPaddingTB(), Unit.PX);
    getElement().getStyle().setPaddingBottom(style.getIssueItemPaddingTB(), Unit.PX);
    ratingElement = factory.createRatingElement(data);
    controlElement = factory.createControlElement(data);
    statusElement = factory.createStatusElement(data);
    
    add(ratingElement);
    add(controlElement);
    add(statusElement);
    
    setCellHorizontalAlignment(statusElement, HasAlignment.ALIGN_RIGHT);
    setCellWidth(controlElement, "100%");
    addMouseOverHandler(new MouseOverHandler() {
      @Override
      public void onMouseOver(MouseOverEvent event) {
        if (STATE.OVER != mousePosition) updateState(STATE.OVER);
      }
    });
    addMouseOutHandler(new MouseOutHandler() {
      @Override
      public void onMouseOut(MouseOutEvent event) {
        if (STATE.OUT != mousePosition) updateState(STATE.OUT);
      }
    });
    
    updateState(STATE.OUT);
  }
  
  private void updateState(STATE pos) {
    mousePosition = pos;
    getElement().getStyle().setProperty(
        "borderBottom",
        style.getIssueItemSeparatorThickness() + "px " + style.getIssueItemSeparatorType() + " "
            + pos.issueItemSeparatorColor);
    getElement().getStyle().setBackgroundColor(pos.backgroundColor);
    
    ratingElement.updateState(pos);
    controlElement.updateState(pos);
    statusElement.updateState(pos);
  }
  
  public void addMouseOverHandler(MouseOverHandler mouseOverHandler) {
    addDomHandler(mouseOverHandler, MouseOverEvent.getType());
  }
  
  public void addMouseOutHandler(MouseOutHandler mouseOutHandler) {
    addDomHandler(mouseOutHandler, MouseOutEvent.getType());
  }
}
