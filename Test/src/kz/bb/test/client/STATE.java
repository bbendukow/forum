package kz.bb.test.client;

public enum STATE {
  OVER, OUT;
  
  public Integer fontSize;
  public String fontColor;
  public String fontStyle;
  public String fontFamily;
  public String fontWeight;
  public String backgroundColor;
  public Integer buttonFontSize;
  public String buttonFontColor;
  public String buttonFontStyle;
  public String buttonFontFamily;
  public String buttonFontWeight;
  public Integer gridCommentsLabelFontSize;
  public String gridCommentsLabelFontColor;
  public String gridCommentsLabelFontStyle;
  public String gridCommentsLabelFontFamily;
  public String gridCommentsLabelFontWeight;
  public Integer gridStatusFontSize;
  public String gridStatusFontColor;
  public String gridStatusFontStyle;
  public String gridStatusFontFamily;
  public String gridStatusFontWeight;
  public String gridStatusBackgroundColor;
  public Integer gridRatingFontSize;
  public String gridRatingFontColor;
  public String gridRatingFontStyle;
  public String gridRatingFontFamily;
  public String gridRatingFontWeight;
  public Integer gridVoicesFontSize;
  public String gridVoicesPositiveFontColor;
  public String gridVoicesPositiveFontStyle;
  public String gridVoicesPositiveFontFamily;
  public String gridVoicesPositiveFontWeight;
  public String gridVoicesNegativeFontColor;
  public String gridVoicesNegativeFontStyle;
  public String gridVoicesNegativeFontFamily;
  public String gridVoicesNegativeFontWeight;
  public Integer gridTitleFontSize;
  public String gridTitleFontColor;
  public String gridTitleFontStyle;
  public String gridTitleFontFamily;
  public String gridTitleFontWeight;
  public String issueItemRatingIndicatorPositiveColor;
  public String issueItemRatingIndicatorNegativeColor;
  public String issueItemRatingIndicatorPositiveBorderColor;
  public String issueItemRatingIndicatorNegativeBorderColor;
  public String issueItemSeparatorColor;
  
  public void init(Integer fontSize, String fontColor, String fontStyle, String fontFamily,
      String fontWeight, String backgroundColor, Integer buttonFontSize, String buttonFontColor,
      String buttonFontStyle, String buttonFontFamily, String buttonFontWeight,
      Integer gridCommentsLabelFontSize, String gridCommentsLabelFontColor,
      String gridCommentsLabelFontStyle, String gridCommentsLabelFontFamily,
      String gridCommentsLabelFontWeight, Integer gridStatusFontSize, String gridStatusFontColor,
      String gridStatusFontStyle, String gridStatusFontFamily, String gridStatusFontWeight,
      String gridStatusBackgroundColor, Integer gridRatingFontSize, String gridRatingFontColor,
      String gridRatingFontStyle, String gridRatingFontFamily, String gridRatingFontWeight,
      Integer gridVoicesFontSize, String gridVoicesPositiveFontColor,
      String gridVoicesPositiveFontStyle, String gridVoicesPositiveFontFamily,
      String gridVoicesPositiveFontWeight, String gridVoicesNegativeFontColor,
      String gridVoicesNegativeFontStyle, String gridVoicesNegativeFontFamily,
      String gridVoicesNegativeFontWeight, Integer gridTitleFontSize, String gridTitleFontColor,
      String gridTitleFontStyle, String gridTitleFontFamily, String gridTitleFontWeight,
      String issueItemRatingIndicatorPositiveColor, String issueItemRatingIndicatorNegativeColor,
      String issueItemRatingIndicatorPositiveBorderColor,
      String issueItemRatingIndicatorNegativeBorderColor, String issueItemSeparatorColor) {
    this.fontSize = fontSize;
    this.fontColor = fontColor;
    this.fontStyle = fontStyle;
    this.fontFamily = fontFamily;
    this.fontWeight = fontWeight;
    this.backgroundColor = backgroundColor;
    this.buttonFontSize = buttonFontSize;
    this.buttonFontColor = buttonFontColor;
    this.buttonFontStyle = buttonFontStyle;
    this.buttonFontFamily = buttonFontFamily;
    this.buttonFontWeight = buttonFontWeight;
    this.gridCommentsLabelFontSize = gridCommentsLabelFontSize;
    this.gridCommentsLabelFontColor = gridCommentsLabelFontColor;
    this.gridCommentsLabelFontStyle = gridCommentsLabelFontStyle;
    this.gridCommentsLabelFontFamily = gridCommentsLabelFontFamily;
    this.gridCommentsLabelFontWeight = gridCommentsLabelFontWeight;
    this.gridStatusFontSize = gridStatusFontSize;
    this.gridStatusFontColor = gridStatusFontColor;
    this.gridStatusFontStyle = gridStatusFontStyle;
    this.gridStatusFontFamily = gridStatusFontFamily;
    this.gridStatusFontWeight = gridStatusFontWeight;
    this.gridStatusBackgroundColor = gridStatusBackgroundColor;
    this.gridRatingFontSize = gridRatingFontSize;
    this.gridRatingFontColor = gridRatingFontColor;
    this.gridRatingFontStyle = gridRatingFontStyle;
    this.gridRatingFontFamily = gridRatingFontFamily;
    this.gridRatingFontWeight = gridRatingFontWeight;
    this.gridVoicesFontSize = gridVoicesFontSize;
    this.gridVoicesPositiveFontColor = gridVoicesPositiveFontColor;
    this.gridVoicesPositiveFontStyle = gridVoicesPositiveFontStyle;
    this.gridVoicesPositiveFontFamily = gridVoicesPositiveFontFamily;
    this.gridVoicesPositiveFontWeight = gridVoicesPositiveFontWeight;
    this.gridVoicesNegativeFontColor = gridVoicesNegativeFontColor;
    this.gridVoicesNegativeFontStyle = gridVoicesNegativeFontStyle;
    this.gridVoicesNegativeFontFamily = gridVoicesNegativeFontFamily;
    this.gridVoicesNegativeFontWeight = gridVoicesNegativeFontWeight;
    this.gridTitleFontSize = gridTitleFontSize;
    this.gridTitleFontColor = gridTitleFontColor;
    this.gridTitleFontStyle = gridTitleFontStyle;
    this.gridTitleFontFamily = gridTitleFontFamily;
    this.gridTitleFontWeight = gridTitleFontWeight;
    this.issueItemRatingIndicatorPositiveColor = issueItemRatingIndicatorPositiveColor;
    this.issueItemRatingIndicatorNegativeColor = issueItemRatingIndicatorNegativeColor;
    this.issueItemRatingIndicatorPositiveBorderColor = issueItemRatingIndicatorPositiveBorderColor;
    this.issueItemRatingIndicatorNegativeBorderColor = issueItemRatingIndicatorNegativeBorderColor;
    this.issueItemSeparatorColor = issueItemSeparatorColor;
  }
  
}
