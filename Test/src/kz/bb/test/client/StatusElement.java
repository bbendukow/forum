package kz.bb.test.client;

import kz.bb.test.client.dto.StatusDTO;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class StatusElement extends VerticalPanel {
  private Label value;
  
  public StatusElement(Factory factory, StatusDTO status) {
    setSize(factory.getStyle().getIssueItemStatusElementWidth() + "px", "100%");
    value = new Label(status.getName());
    add(value);
    setCellHorizontalAlignment(value, HasAlignment.ALIGN_RIGHT);
    setCellVerticalAlignment(value, HasAlignment.ALIGN_MIDDLE);
  }
  
  public void updateState(STATE pos) {
    Element w = value.getElement();
    w.getStyle().setProperty("color", pos.gridStatusFontColor);
    w.getStyle().setFontSize(pos.gridStatusFontSize, Unit.PX);
    w.getStyle().setProperty("fontStyle", pos.gridStatusFontStyle);
    w.getStyle().setProperty("fontWeight", pos.gridStatusFontWeight);
    w.getStyle().setProperty("fontFamily", pos.gridStatusFontFamily);
    value.getElement().getStyle().setBackgroundColor(pos.gridStatusBackgroundColor);
  }
}
